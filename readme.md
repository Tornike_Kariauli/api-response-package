## Introduction

Library for extending Laravel Response facade

## Installation

To get started simply run:

    composer require app3null/api-response

## Basic Usage

#Return success response:

    return response()->success([...data]);

    return response()->success([], 204);

#Return error response:

    return response()->fail($http_code, $message);

## License

The library is an open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
